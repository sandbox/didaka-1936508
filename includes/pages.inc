<?php
/**
 * @file
 */

/**
 * Donations report page
 *
 * @return sting
 *   Donations report page html
 */
function donations_status_page_causes() {
  $page = '';
  $header = array(
    array(
      'data' => t('Cause'),
      'field' => 'title',
    ),
    array(
      'data' => t('Raised'),
      'field' => 'current_total',
    ),
    array(
      'data' => t('Goal'),
      'field' => 'total_goal',
    ),
    array(
      'data' => t('Last until'),
      'field' => 'last_until',
    ),
    array(
      'data' => t('Active'),
      'field' => 'active',
    ),
  );
  $resultset = pager_query("SELECT node_revisions.title, node_revisions.nid, donations_causes.current_total, donations_causes.total_goal, donations_causes.last_until, donations_causes.active FROM {donations_causes} LEFT JOIN node ON donations_causes.nid = node.nid LEFT JOIN node_revisions ON donations_causes.vid = node_revisions.vid WHERE donations_causes.vid = node.vid" . tablesort_sql($header), 30);
  $data = array();
  while ($row = db_fetch_array($resultset)) {
    $row['title'] = l($row['title'], 'admin/reports/donations/causes/' . $row['nid']);
    unset($row['nid']);
    $date = date('d-m-Y', $row['last_until']);
    $row['last_until'] = $date;
    $row['active'] = $row['active'] == 1 ? t('Yes') : t('No');
    $data[$i] = $row;
    $i++;
  };

  $table = theme_table($header, $data, $atr);
  $table .= theme('pager');
  $page .= $table;
  drupal_set_title(t('Causes'));
  return $page;
}
/**
 * Lists all donations for a certain cause.
 */
function donations_status_page_cause($id) {
  $page = '';
  $header = array(
    t('Cause'),
    t('Raised'),
    t('Goal'),
    t('Last until'),
    t('Active'),
  );
  $resultset = db_query("SELECT node_revisions.title, donations_causes.nid, donations_causes.current_total, donations_causes.total_goal, donations_causes.last_until, donations_causes.active FROM {donations_causes} LEFT JOIN node ON donations_causes.nid = node.nid LEFT JOIN node_revisions ON donations_causes.vid = node_revisions.vid WHERE donations_causes.nid = '%d' AND donations_causes.vid = node.vid", $id);
  $data = array();
  while ($row = db_fetch_array($resultset)) {
    $cause_title = $row['title'];
    $row['title'] = l($row['title'], 'admin/reports/donations/causes/' . $row['nid']);
    unset($row['nid']);
    $date = date('d-m-Y', $row['last_until']);
    $row['last_until'] = $date;
    $row['active'] = $row['active'] == 1 ? t('Yes') : t('No');
    $data[$i] = $row;
    $i++;
  };
  $table = theme_table($header, $data, $atr);
  $page .= $table;
  $header = array(
    array(
      'data' => t('Donation ID'),
      'field' => 'donation_id',
      'sort' => 'desc',
    ),
    array(
      'data' => t('Donor'),
      'field' => 'name',
    ),
    array(
      'data' => t('Amount'),
      'field' => 'amount',
    ),
    array(
      'data' => t('Currency'),
      'field' => 'currency',
    ),
    array(
      'data' => t('Method'),
      'field' => 'method',
    ),
    array(
      'data' => t('Date'),
      'field' => 'date',
    ),
    array(
      'data' => t('Document'),
      'field' => 'document',
    ),
    array(
      'data' => t('Anonymous'),
      'field' => 'anonymous',
    ),
  );
  if (is_numeric($id)) {
    $resultset = pager_query("SELECT donations_donations.donation_id, donations_donations.donor_id, donations_donors.name, donations_donations.amount, donations_donations.currency, donations_donations.method, donations_donations.date, donations_donations.document, donations_donations.anonymous FROM {donations_donations} INNER JOIN donations_donors ON donations_donations.donor_id = donations_donors.donor_id WHERE donations_donations.nid = $id" . tablesort_sql($header), $limit = 30);
  }
  else {

  }
  $data = array();

  while ($donations_rows = db_fetch_array($resultset)) {
    $donations_rows['donation_id'] = l($donations_rows['donation_id'], 'admin/reports/donations/donations/' . $donations_rows['donation_id']);
    $donations_rows['name'] = l($donations_rows['name'], 'admin/reports/donations/donors/' . $donations_rows['donor_id']);
    unset($donations_rows['donor_id']);
    $donations_rows['date'] = date('d-m-Y', $donations_rows['date']);
    $donations_rows['method'] = _donations_method_labels($donations_rows['method']);
    $donations_rows['document'] = _donations_yes_no_helper($donations_rows['document']);
    $donations_rows['anonymous'] = _donations_yes_no_helper($donations_rows['anonymous']);
    $data[$i] = $donations_rows;
    $i++;
  };


  $donations_table = theme_table($header, $data, $atr);
  $donations_table .= theme('pager');
  $page .= '<fieldset><legend><strong>' . t('List of all donations for this cause') . '</strong></legend>';
  $page .= $donations_table;
  $page .= '</fieldset>';
  drupal_set_title(t('Donations for %name', array('%name' => $cause_title)));
  return $page;
}
/**
 * Donations summary List of all donations
 */
function donations_status_page_donations() {
  $page = '';
  $header = array(
    array(
      'data' => t('Cause ID'),
      'field' => 'donation_id',
      'sort' => 'desc',
    ),
    array(
      'data' => t('Cause'),
      'field' => 'nid',
    ),
    array(
      'data' => t('Donor ID'),
      'field' => 'donor_id',
    ),
    array(
      'data' => t('Amount'),
      'field' => 'amount',
    ),
    array(
      'data' => t('Currency'),
      'field' => 'currency',
    ),
    array(
      'data' => t('Method'),
      'field' => 'method',
    ),
    array(
      'data' => t('Date'),
      'field' => 'date',
    ),
    array(
      'data' => t('Document'),
      'field' => 'document',
    ),
    array(
      'data' => t('Anonymous'),
      'field' => 'anonymous',
    ),
  );
  $resultset = pager_query("SELECT donation_id, nid, donor_id, amount, currency, method , date, document, anonymous FROM {donations_donations}" . tablesort_sql($header), $limit = 30);
  $data = array();
  while ($row = db_fetch_array($resultset)) {
    $row['donation_id'] = l($row['donation_id'], 'admin/reports/donations/donations/' . $row['donation_id']);
    $row['nid'] = l(node_load($row['nid'])->title, 'admin/reports/donations/causes/' . $row['nid']);
    $row['donor_id'] = l($row['donor_id'], 'admin/reports/donations/donors/' . $row['donor_id']);
    $row['method'] = _donations_method_labels($row['method']);
    $row['date'] = date('d-m-Y', $row['date']);
    $row['document'] = _donations_yes_no_helper($row['document']);
    $row['anonymous'] = _donations_yes_no_helper($row['anonymous']);
    $data[$i] = $row;
    $i++;
  };
  $table = theme_table($header, $data, $atr);
  $table .= theme('pager');
  $page .= $table;
  drupal_set_title(t('List of all donations'));
  return $page;
}

/**
 * Report page for a single donations transaction
 */
function donations_status_page_donation($id) {
  $page = '';
  if (is_numeric($id)) {
    $donation = db_fetch_array(db_query("SELECT nid, amount, currency, method, donor_id, document, anonymous, date FROM {donations_donations} WHERE donation_id = '%d'", $id));
    if (isset($donation['nid'])) {
      $donor = db_fetch_array(db_query("SELECT name, email, phone FROM {donations_donors} WHERE donor_id = '%d'", $donation['donor_id']));
      $node = node_load($donation['nid']);
      $page = '<fieldset><legend><strong>' . t('Donation') . '</strong></legend>';
      $page .= '<div class="row"><label class="inline">' . t('Donation ID') . ': </label><span>' . $id . '</span></div>';
      $page .= '<div class="row"><label class="inline">' . t('Cause') . ': </label><span>' . l($node->title, 'node/' . $donation['nid']) . '</span></div>';
      $page .= '<div class="row"><label class="inline">' . t('Donation amount') . ': </label><span>' . $donation['amount'] . ' ' . $donation['currency'] . '</span></div>';
      $page .= '<div class="row"><label class="inline">' . t('Payment method') . ': </label><span>' . _donations_method_labels($donation['method']) . '</span></div>';
      $page .= '<div class="row"><label class="inline">' . t('The donor wants to receive a document for this donation') . ': </label><span>' . _donations_yes_no_helper($donation['document']) . '</span></div>';
      $page .= '<div class="row"><label class="inline">' . t('The donor wants to be anonymous') . ': </label><span>' . _donations_yes_no_helper($donation['anonymous']) . '</span></div>';
      $page .= '</fieldset>';
      $page .= '<fieldset><legend><strong>' . t("Donor's information") . '</strong></legend>';
      $page .= '<div class="row"><label class="inline">' . t('Name') . ': </label><span>' . l($donor['name'], 'admin/reports/donations/donors/' . $donation['donor_id']) . '</span></div>';
      $page .= '<div class="row"><label class="inline">' . t('E-mail') . ': </label><span>' . $donor['email'] . '</span></div>';
      $page .= '<div class="row"><label class="inline">' . t('Phone') . ': </label><span>' . $donor['phone'] . '</span></div>';
      $page .= '</fieldset>';
      drupal_set_title(t('Details for donation #!id', array('!id' => $id)));
    }
    else {
      drupal_set_message(t('Invalid donation ID'), 'error');
    }
  }
  else {
    drupal_set_message(t('Invalid choice'), 'error');
  }
  return $page;
}
/**
 * Donor summary page
 */
function donations_status_page_donors() {
  $page = '';
  $header = array(
    array(
      'data' => t('Name'),
      'field' => 'name',
    ),
    array(
      'data' => t('Donor ID'),
      'field' => 'donor_id',
      'sort' => 'decs',
    ),
    array(
      'data' => t('E-mail'),
      'field' => 'email',
    ),
    array(
      'data' => t('Phone'),
      'field' => 'phone',
    ),
  );
  $resultset = db_query("SELECT name, donor_id, email, phone FROM {donations_donors}" . tablesort_sql($header));
  $data = array();
  while ($row = db_fetch_array($resultset)) {
    $name = $row['name'];
    $row['name'] = l($name, 'admin/reports/donations/donors/' . $row['donor_id']);
    $data[$i] = $row;
    $i++;
  };
  $table = theme_table($header, $data, $atr);
  $page .= $table;
  drupal_set_title(t('List of all Donors'));
  return $page;
}

/**
 * Donor page
 */
function donations_status_page_donor($donor_id) {
  $page = '';

  if (is_numeric($donor_id)) {
    $donor = db_fetch_array(db_query("SELECT name, donor_id, email, phone FROM {donations_donors} WHERE donor_id = '%d'", $donor_id));
    $page .= '<fieldset><legend><strong>' . t("Donor's information") . '</strong></legend>';
    $page .= '<div class="row"><label class="inline">' . t('Name') . ': </label><span>' . $donor['name'] . '</span></div>';
    $page .= '<div class="row"><label class="inline">' . t('E-mail') . ': </label><span>' . $donor['email'] . '</span></div>';
    $page .= '<div class="row"><label class="inline">' . t('Phone') . ': </label><span>' . $donor['phone'] . '</span></div>';
    $page .= '</fieldset>';

    $header = array(
      array(
        'data' => t('Donation ID'),
        'field' => 'donation_id',
        'sort' => 'decs',
      ),
      t('Cause'),
      array(
        'data' => t('Amount'),
        'field' => 'amount',
      ),
      array(
        'data' => t('Currency'),
        'field' => 'currency',
      ),
      array(
        'data' => t('Method'),
        'field' => 'method',
      ),
      array(
        'data' => t('Date'),
        'field' => 'date',
      ),
      array(
        'data' => t('Document'),
        'field' => 'document',
      ),
      array(
        'data' => t('Anonymous'),
        'field' => 'anonymous',
      ),
    );
    $resultset = db_query("SELECT donation_id, nid, amount, currency, method , date, document, anonymous FROM {donations_donations} WHERE donor_id = '%d'", $donor_id);
    $donations = array();
    while ($donations_rows = db_fetch_array($resultset)) {
      $donations_rows['donation_id'] = l($donations_rows['donation_id'], 'admin/reports/donations/donations/' . $donations_rows['donation_id']);
      $donations_rows['nid'] = l(node_load($donations_rows['nid'])->title, 'admin/reports/donations/causes/' . $donations_rows['nid']);
      $donations_rows['method'] = _donations_method_labels($donations_rows['method']);
      $donations_rows['date'] = date('d-m-Y', $donations_rows['date']);
      $donations_rows['document'] = _donations_yes_no_helper($donations_rows['document']);
      $donations_rows['anonymous'] = _donations_yes_no_helper($donations_rows['anonymous']);
      $donations[$i] = $donations_rows;
      $i++;
    };
    $donations_table = theme_table($header, $donations, $atr);
    $page .= '<fieldset><legend><strong>' . t('Donations from this donor') . '</strong></legend>';
    $page .= $donations_table;
    $page .= '</fieldset>';
    $title = t('Details for %name', array('%name' => $donor['name']));
    drupal_set_title($title);
  }
  return $page;
}
