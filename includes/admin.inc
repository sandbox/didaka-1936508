<?php
/**
 * @file
 * This file contaions the administration forms.
 */

/**
 * Creataing admin form for donations_admin().
 */
function donations_admin() {
  $form = array();

  $form['node_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['node_types']['donations_node_types'] = array(
    '#type' => 'checkboxes',
    '#description' => t('Select types that will use Donations advanced'),
    '#default_value' => variable_get('donations_node_types', array()),
    '#options' => node_get_types('names'),
  );
  $form['weight'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('donations_weight', '1'),
    '#maxlength' => 2,
    '#size' => 2,
    '#description' => t('Set larger value to make the donation form displey lower in the node display'),
    '#prefix' => '<fieldset><legend>' . t('Weight on node view') . '</legend>',
    '#suffix' => '</fieldset>',
  );
  $form['payment_gatways'] = array(
    '#type' => 'fieldset',
    '#title' => t('Payment gateways'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // Bank transfer details.
  $form['payment_gatways']['bank_transfer'] = array(
    '#type' => 'fieldset',
    '#title' => t('Detail for bank transfer'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['payment_gatways']['bank_transfer']['donations_bank_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Bank name'),
    // FIXME: Make the variable donations_bank_transfer_instructions
    // multilingual.
    '#default_value' => variable_get('donations_bank_name', ''),
  );
  $form['payment_gatways']['bank_transfer']['donations_bank_bic'] = array(
    '#type' => 'textfield',
    '#title' => t('BIC (SWIFT)'),
    '#default_value' => variable_get('donations_bank_bic', ''),
  );
  $form['payment_gatways']['bank_transfer']['donations_bank_transfer_iban_bgn'] = array(
    '#type' => 'textfield',
    '#title' => t('IBAN BGN'),
    '#default_value' => variable_get('donations_bank_transfer_iban_bgn', ''),
    '#prefix' => '<fieldset><legend>' . t('Details for BGN bank account') . '</legend>',
    '#suffix' => '</fieldset>',
  );

  $form['payment_gatways']['bank_transfer']['donations_bank_transfer_iban_usd'] = array(
    '#type' => 'textfield',
    '#title' => t('IBAN USD'),
    '#default_value' => variable_get('donations_bank_transfer_iban_usd', ''),
    '#prefix' => '<fieldset><legend>' . t('Details for USD bank account') . '</legend>',
    '#suffix' => '</fieldset>',
  );
  $form['payment_gatways']['bank_transfer']['donations_bank_transfer_instructions_filter']['donations_bank_transfer_instructions'] = array(
    '#type' => 'textarea',
    '#title' => t('Instructions to donor'),
    // TODO: Make the variable donations_bank_transfer_instructions
    // multilingual.
    '#default_value' => variable_get('donations_bank_transfer_instructions', ''),
  );
  if (!isset($donations_bank_transfer_instructions_filter)) {
    $donations_bank_transfer_instructions_filter = variable_get('donations_bank_transfer_instructions_filter', FILTER_FORMAT_DEFAULT);
  }
  $form['payment_gatways']['bank_transfer']['donations_bank_transfer_instructions_filter']['format'] = filter_form($donations_bank_transfer_instructions_filter, NULL, array('donations_bank_transfer_instructions_filter'));
  // ePay.bg details.
  $form['payment_gatways']['epaybg'] = array(
    '#type' => 'fieldset',
    '#title' => t('ePay.bg'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['payment_gatways']['epaybg']['donations_epaybg_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Gateway mode'),
    '#default_value' => variable_get('donations_epaybg_mode', 'https://www.epay.bg/'),
    '#options' => array(
      'https://www.epay.bg/' => t('Live site'),
      'https://devep2.datamax.bg/ep2/epay2_demo/' => t('Sandbox site'),
    ),
    '#required' => TRUE,
  );
  $form['payment_gatways']['epaybg']['donations_epaybg_min'] = array(
    '#type' => 'textfield',
    '#title' => t('MIN (Merchant Identification Number)'),
    '#default_value' => variable_get('donations_epaybg_min', 'Enter MIN'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['payment_gatways']['epaybg']['donations_epaybg_secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret key'),
    '#default_value' => variable_get('donations_epaybg_secret_key', 'Enter Secret key'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['payment_gatways']['epaybg']['donations_epaybg_url_ok'] = array(
    '#type' => 'textfield',
    '#title' => t('URL_OK'),
    '#default_value' => variable_get('donations_epaybg_url_ok', 'Enter URL_OK'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['payment_gatways']['epaybg']['donations_epaybg_url_cancel'] = array(
    '#type' => 'textfield',
    '#title' => t('URL_CANCEL'),
    '#default_value' => variable_get('donations_epaybg_url_cancel', 'Enter URL_CANCEL'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['payment_gatways']['epaybg']['donations_epaybg_instructions_filter']['donations_epaybg_instructions'] = array(
    '#type' => 'textarea',
    '#title' => t('Instructions to donor when using ePay.bg portal'),
    // TODO: Make the variable donations_bank_transfer_instructions
    // multilingual.
    '#default_value' => variable_get('donations_epaybg_instructions', ''),
  );
  if (!isset($donations_epaybg_instructions_filter)) {
    $donations_epaybg_instructions_filter = variable_get('donations_epaybg_instructions_filter', FILTER_FORMAT_DEFAULT);
  }
  $form['payment_gatways']['epaybg']['donations_epaybg_instructions_filter']['format'] = filter_form($donations_epaybg_instructions_filter, NULL, array('donations_epaybg_instructions_filter'));

  $form['payment_gatways']['epaybg']['donations_cc_instructions_filter']['donations_cc_instructions'] = array(
    '#type' => 'textarea',
    '#title' => t('Instructions to donor for credit card payments'),
    // TODO: Make the variable donations_bank_transfer_instructions
    // multilingual.
    '#default_value' => variable_get('donations_cc_instructions', ''),
  );
  if (!isset($donations_cc_instructions_filter)) {
    $donations_cc_instructions_filter = variable_get('donations_cc_instructions_filter', FILTER_FORMAT_DEFAULT);
  }
  $form['payment_gatways']['epaybg']['donations_cc_instructions_filter']['format'] = filter_form($donations_cc_instructions_filter, NULL, array('donations_cc_instructions_filter'));
  // PayPal details.
  $form['payment_gatways']['paypal'] = array(
    '#type' => 'fieldset',
    '#title' => t('PayPal'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // PayPal details.
  $form['payment_gatways']['paypal'] = array(
    '#type' => 'fieldset',
    '#title' => t('PayPal'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['payment_gatways']['paypal']['donations_paypal_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Gateway mode'),
    '#default_value' => variable_get('donations_paypal_mode', 'https://www.paypal.com/cgi-bin/webscr'),
    '#options' => array(
      'https://www.paypal.com/cgi-bin/webscr' => t('Live site'),
      'https://www.sandbox.paypal.com/cgi-bin/webscr' => t('Sandbox site'),
    ),
    '#required' => TRUE,
  );
  $form['payment_gatways']['paypal']['donations_paypal_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter your PayPal ID or an email address'),
    '#default_value' => variable_get('donations_paypal_id', 'PayPal ID'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#description' => t('Your PayPal ID or an email address associated with your PayPal account. Email addresses must be confirmed.'),
  );
  $form['payment_gatways']['paypal']['donations_paypal_url_return'] = array(
    '#type' => 'textfield',
    '#title' => t('URL_OK'),
    '#default_value' => variable_get('donations_paypal_url_return', 'Enter URL_OK'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#description' => t('The URL to which PayPal redirects donors’ browser after they complete their payments. For example, specify a URL on your site that displays a “Thank you for your donation” page.'),
  );
  $form['payment_gatways']['paypal']['donations_paypal_url_cancel_return'] = array(
    '#type' => 'textfield',
    '#title' => t('URL_CANCEL'),
    '#default_value' => variable_get('donations_paypal_url_cancel_return', 'Enter URL_CANCEL'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#description' => t('A URL to which PayPal redirects the donors’ browsers if they cancel checkout before completing their payments. For example, specify a URL on your website that displays a “Donation Canceled” page.'),
  );
  $form['payment_gatways']['paypal']['donations_paypal_no_note'] = array(
    '#type' => 'radios',
    '#title' => t('Allow donors to include a note with their donations'),
    '#default_value' => variable_get('donations_paypal_no_note', '0'),
    '#options' => array(
      '0' => t('Yes'),
      '1' => t('No'),
    ),
    '#required' => TRUE,
  );
  $form['payment_gatways']['paypal']['donations_paypal_instructions_filter']['donations_paypal_instructions'] = array(
    '#type' => 'textarea',
    '#title' => t('Instructions to donor when using PayPal portal'),
    // TODO: Make the variable donations_bank_transfer_instructions
    // multilingual.
    '#default_value' => variable_get('donations_paypal_instructions', ''),
  );
  if (!isset($donations_paypal_instructions_filter)) {
    $donations_paypal_instructions_filter = variable_get('donations_paypal_instructions_filter', FILTER_FORMAT_DEFAULT);
  }
  $form['payment_gatways']['paypal']['donations_paypal_instructions_filter']['format'] = filter_form($donations_paypal_instructions_filter, NULL, array('donations_paypal_instructions_filter'));

  $form['aditional_details'] = array(
    '#type' => 'fieldset',
    '#title' => t('Additonal details'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('You can use the following tokens to compese the summary and the e-mails.</ br>
      <pre>
      /// Donation detail ///
      !cause - Cause title
      !donation_id - The ID for the donation transaction
      !method - The selected payment method
      !donation_amount - The donation amount

      /// Donor detail ///
      !donor_name - The donor\'s name
      !donor_email - The donor\'s email address
      !donor_phone - The donor\'s phone
      !donor_document - If the donor wants document dor the donation; the options are Yes and No
      !donor_anonymous - If the donor wants to stay anonymous; the options are Yes and No

      /// Bank transfer spacific tokens ///
      !bank_name - Bank name
      !bank_bic - BIC/SWIFT code
      !bank_iban - IBAN based on the sellected currency
      </pre>
      '),
  );
  $form['aditional_details']['summary'] = array(
    '#type' => 'fieldset',
    '#title' => t('Summary'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['aditional_details']['summary']['donations_summary_filter']['donations_summary'] = array(
    '#type' => 'textarea',
    '#title' => t('Summary'),
    '#default_value' => variable_get('donations_summary', ''),
  );
  if (!isset($donations_summary_filter)) {
    $donations_summary_filter = variable_get('donations_summary_filter', FILTER_FORMAT_DEFAULT);
  }
  $form['aditional_details']['summary']['donations_summary_filter']['format'] = filter_form($donations_summary_filter, NULL, array('donations_summary_filter'));

  // TODO: Create a loop for e-mail templates based on the payment methods!
  // FIXME: Add more tokens for bank transfet! IBAN, Bank name etc.
  $form['aditional_details']['emails'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email templates'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['aditional_details']['emails']['donations_admin_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Admin e-mail'),
    '#description' => t('All notificatios for donations will be sent to this e-mail. You can add additonal e-mails separated by commas.'),
    '#default_value' => variable_get('donations_admin_email', variable_get('site_mail', 'admin@example.com')),
  );
  $form['aditional_details']['emails']['donations_from_email'] = array(
    '#type' => 'textfield',
    '#title' => t('From e-mail'),
    '#description' => t('You can specify from which e-mail address the donors will receive messages.'),
    '#default_value' => variable_get('donations_from_email', variable_get('donations_admin_email', variable_get('site_mail', 'admin@example.com'))),
  );
  $form['aditional_details']['emails']['donations_email_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject for e-mails sent to donors'),
    '#default_value' => variable_get('donations_email_subject', 'Thank you for donating!'),
    '#description' => t('You can use the tokens above.'),
  );
  $form['aditional_details']['emails']['bank_transfer'] = array(
    '#type' => 'fieldset',
    '#title' => t('For bank transfer'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['aditional_details']['emails']['bank_transfer']['donations_bank_transfer_donor_email'] = array(
    '#type' => 'textarea',
    '#title' => t('E-mail sent to donor when using bank transfer'),
    '#default_value' => variable_get('donations_bank_transfer_donor_email', ''),
    '#description' => t('You can use the tokens above to compose the e-mail.'),
  );
  $form['aditional_details']['emails']['epaybg'] = array(
    '#type' => 'fieldset',
    '#title' => t('For ePay.bg'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['aditional_details']['emails']['epaybg']['donations_epaybg_donor_email'] = array(
    '#type' => 'textarea',
    '#title' => t('E-mail sent to donor when using ePay.bg'),
    '#default_value' => variable_get('donations_epaybg_donor_email', ''),
    '#description' => t('You can use the tokens above to compose the e-mail.'),
  );
  $form['aditional_details']['emails']['paypal'] = array(
    '#type' => 'fieldset',
    '#title' => t('For PayPal'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['aditional_details']['emails']['paypal']['donations_paypal_donor_email'] = array(
    '#type' => 'textarea',
    '#title' => t('E-mail sent to donor when using PayPal'),
    '#default_value' => variable_get('donations_paypal_donor_email', ''),
    '#description' => t('You can use the tokens above to compose the e-mail.'),
  );
  $form['aditional_details']['emails']['cc'] = array(
    '#type' => 'fieldset',
    '#title' => t('For credit cards'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['aditional_details']['emails']['cc']['donations_cc_donor_email'] = array(
    '#type' => 'textarea',
    '#title' => t('E-mail sent to donor when using credit cards'),
    '#default_value' => variable_get('donations_cc_donor_email', ''),
    '#description' => t('You can use the tokens above to compose the e-mail.'),
  );
  $form['aditional_details']['donor_help'] = array(
    '#type' => 'fieldset',
    '#title' => t('Help for donors'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['aditional_details']['donor_help']['donations_document_donor_help_filter']['donations_document_donor_help'] = array(
    '#type' => 'textarea',
    '#title' => t('Help text for document'),
    '#default_value' => variable_get('donations_document_donor_help', ''),
  );
  if (!isset($donations_document_donor_help_filter)) {
    $donations_document_donor_help_filter = variable_get('donations_document_donor_help_filter', FILTER_FORMAT_DEFAULT);
  }
  $form['aditional_details']['donor_help']['donations_document_donor_help_filter']['format'] = filter_form($donations_document_donor_help_filter, NULL, array('donations_document_donor_help_filter'));

  $form['aditional_details']['donor_help']['donations_anonym_donor_help_filter']['donations_anonym_donor_help'] = array(
    '#type' => 'textarea',
    '#title' => t('Help text for anonymous donors'),
    '#default_value' => variable_get('donations_anonym_donor_help', ''),
  );
  if (!isset($donations_anonym_donor_help_filter)) {
    $donations_anonym_donor_help_filter = variable_get('donations_anonym_donor_help_filter', FILTER_FORMAT_DEFAULT);
  }
  $form['aditional_details']['donor_help']['donations_anonym_donor_help_filter']['format'] = filter_form($donations_anonym_donor_help_filter, NULL, array('donations_anonym_donor_help_filter'));
  return system_settings_form($form);
}
