<?php
/**
 * @file
 */

/**
 * PayPal sumary form
 *
 * @param array $form_state
 *   form state
 *
 * @return array
 *   PayPal form array
 */
function donations_paypal_form($form_state) {

  extract($form_state['storage']['donation']);
  extract($form_state['storage']['donor']);
  $descr = drupal_substr(t('Donation for !title', array('!title' => $title)), 0, 127);
  $form['progress'] = array(
    '#type' => 'markup',
    '#value' => theme('donation_progress_breadcrb_list', 3),
  );
  $form['summary'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="buttons-box" class="donation-box"><div class="donation-box-inner">',
    '#value' => _donations_confirmation_summary($form_state['storage']['donation_summary']),
    '#suffix' => '</div></div>',
  );
  $form['instructions'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="donation-box"><div class="donation-box-inner">',
    '#value' => variable_get('donations_paypal_instructions', ''),
    '#suffix' => '</div></div>',
  );
  $form['cmd'] = array(
    '#type' => 'hidden',
    '#name' => 'cmd',
    '#value' => '_donations',
  );
  $form['business'] = array(
    '#type' => 'hidden',
    '#name' => 'business',
    '#value' => variable_get('donations_paypal_id', 'PayPal ID'),
  );
  $form['charset'] = array(
    '#type' => 'hidden',
    '#name' => 'charset',
    '#value' => 'utf-8',
  );
  $form['item_number'] = array(
    '#type' => 'hidden',
    '#name' => 'item_number',
    '#value' => $donation_id,
  );
  $form['item_name'] = array(
    '#type' => 'hidden',
    '#name' => 'item_name',
    '#value' => $descr,
  );
  $form['amount'] = array(
    '#type' => 'hidden',
    '#name' => 'amount',
    '#value' => $amount,
  );
  $form['currency_code'] = array(
    '#type' => 'hidden',
    '#name' => 'currency_code',
    '#value' => $currency,
  );
  $form['return'] = array(
    '#type' => 'hidden',
    '#name' => 'return',
    '#value' => variable_get('donations_paypal_url_return', 'Enter URL_OK'),
  );
  $form['cancel_return'] = array(
    '#type' => 'hidden',
    '#name' => 'cancel_return',
    '#value' => variable_get('donations_paypal_url_cancel_return', 'Enter URL_CANCEL'),
  );
  $form['no_note'] = array(
    '#type' => 'hidden',
    '#name' => 'no_note',
    '#value' => variable_get('donations_paypal_no_note', '0'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#prefix' => '<div id="buttons-box" class="donation-box"><div class="donation-box-inner">',
  );
  $form['finish'] = array(
    '#type' => 'submit',
    '#value' => t('Finish'),
    '#suffix' => '</div></div>',
  );

  return $form;
}
