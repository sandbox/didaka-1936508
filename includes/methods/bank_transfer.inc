<?php
/**
 * @file
 */

/**
 * Bank transfer summary form
 */
function donations_bank_transfer_form($form_state) {
  $form = array();
  $form['progress'] = array(
    '#type' => 'markup',
    '#value' => theme('donation_progress_breadcrb_list', 3),
  );
  $form['summary'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="buttons-box" class="donation-box"><div class="donation-box-inner">',
    '#value' => _donations_confirmation_summary($form_state['storage']['donation_summary']),
    '#suffix' => '</div></div>',
  );
  $form['instructions'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="buttons-box" class="donation-box"><div class="donation-box-inner">',
    '#value' => _donations_summary($form_state['storage']['donation_summary'], array('donations_bank_transfer_instructions', '')),
    '#suffix' => '</div></div>',
  );
  $form['cancel'] = array(
    '#prefix' => '<div id="buttons-box" class="donation-box"><div class="donation-box-inner">',
    '#type' => 'submit',
    '#name' => 'cancel',
    '#value' => t('Cancel'),
  );
  $form['finish'] = array(
    '#suffix' => '</div></div>',
    '#type' => 'submit',
    '#name' => 'finish',
    '#value' => t('Finish'),
  );
  return $form;
}
