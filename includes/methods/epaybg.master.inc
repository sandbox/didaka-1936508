<?php
/**
 * @file
 */

/**
 * ePay.bg summary form
 *
 * @param array $form_state
 *   form state
 */
function donations_epaybg_form($form_state) {
  extract($form_state['storage']['donation']);
  extract($form_state['storage']['donor']);
  $min = variable_get('donations_epaybg_min', 'Enter MIN');
  $secret = variable_get('donations_epaybg_secret_key', 'Enter Secret key');
  $exp_date = date('d.m.Y', time() + 60 * 60 * 24 * 30);
  $descr = drupal_substr(t('Donation for !title', array('!title' => $title)), 0, 100);

  global $language;
  $lang_name = $language->language;

  $data = <<<DATA
MIN={$min}
INVOICE={$donation_id}
AMOUNT={$amount}
EXP_TIME={$exp_date}
DESCR={$descr}
CURRENCY={$currency}
ENCODING=utf-8
DATA;

  $ENCODED = base64_encode($data);
  // SHA-1 algorithm REQUIRED!
  $CHECKSUM = _donations_hmac('sha1', $ENCODED, $secret);
  $form['#suffix'] = '</fieldset>';
  $form['progress'] = array(
    '#type' => 'markup',
    '#value' => theme('donation_progress_breadcrb_list', 3),
  );
  $form['summary'] = array(
    '#type' => 'markup',
    '#prefix' => '<fieldset><div>',
    '#value' => _donations_summary($form_state['storage']['donation_summary'], array('donations_summary', '')),
    '#suffix' => '</div></fieldset>',
  );
  $form['instructions'] = array(
    '#type' => 'markup',
    '#prefix' => '<fieldset></legend><div>',
    '#value' => variable_get('donations_epaybg_instructions', ''),
    '#suffix' => '</div></fieldset>',
  );
  $form['page'] = array(
    '#prefix' => '</div></form><fieldset><form action="' . variable_get('donations_epaybg_mode', 'https://www.epay.bg/') . '" accept-charset="UTF-8" method="post">',
    '#type' => 'hidden',
    '#name' => 'PAGE',
    '#value' => 'paylogin',
  );
  $form['LANG'] = array(
    '#type' => 'hidden',
    '#name' => 'LANG',
    '#value' => $lang_name,
  );
  $form['ENCODED'] = array(
    '#type' => 'hidden',
    '#name' => 'ENCODED',
    '#value' => $ENCODED,
  );
  $form['CHECKSUM'] = array(
    '#type' => 'hidden',
    '#name' => 'CHECKSUM',
    '#value' => $CHECKSUM,
  );
  $form['URL_OK'] = array(
    '#type' => 'hidden',
    '#name' => 'URL_OK',
    '#value' => variable_get('donations_epaybg_url_ok', 'Enter URL_OK'),
  );
  $form['URL_CANCEL'] = array(
    '#type' => 'hidden',
    '#name' => 'URL_CANCEL',
    '#value' => variable_get('donations_epaybg_url_cancel', 'Enter URL_CANCEL'),
  );
  $form['finish'] = array(
    '#suffix' => '</form>',
    '#type' => 'submit',
    '#name' => 'finish',
    '#value' => t('Finish'),

  );
  $form['cancel'] = array(
    '#prefix' => '<form action="/donate" accept-charset="UTF-8" method="post" id="donations-donation-form-page-one">',
    '#type' => 'submit',
    '#name' => 'cancel',
    '#value' => t('Cancel'),
  );
  return $form;
}
