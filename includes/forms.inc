<?php
/**
 * @file
 * This file contains the donation form and some functions to process it.
 */

/**
 * PAGE 1 - Donation form
 *
 * This the the frist step in the donation process. The form collect
 * information about the donation inself. The user have to sellect payment
 * method, enter amount to donate and chose a currency.
 * Available currencies are dynimcaly generded based on the selected payment
 * method. This is required, because different payment processor have differnt
 * currencies available.
 *
 *  @param array $form_state
 *   An asotitive aray with the current form state
 *
 *  @param array $node
 *   the node array
 *
 *  @return array
 *   form array to be processed
 */
function donations_donation_form_page_one(&$form_state, $node = NULL) {
  // FIXME: Add check to see if the node form the cookie is an active dontion!
  if (isset($node)) {
    _donations_set_cookie('dontaion_nid', $node->nid);
  }
  elseif (isset($_COOKIE['dontaion_nid'])) {
    $nid = $_COOKIE['dontaion_nid'];
    $node = node_load($nid);
  }
  else {
    $form['no_cause'] = array(
      '#type' => 'markup',
      '#prefix' => '<fieldset>',
      '#suffix' => '</fieldset>',
      // FIXME: Add url for causes page!
      '#value' => '<div class="no-cause">' . t('Please, sellect a cause') . '</div>',
    );
    return $form;
  }

  if ($form_state['storage']['show_page'] == '2') {
    return donations_donation_form_page_two(&$form_state, $node);
  }
  elseif ($form_state['storage']['show_page'] == '3') {
    return donations_donation_form_page_three(&$form_state, $node);
  }

  $form['#action'] = url('donate');

  // Displays the selected cause/
  if (!(arg(0) == 'node' && is_numeric(arg(1)) && !arg(2)) && isset($node)) {
    $form['progress'] = array(
      '#type' => 'markup',
      '#value' => theme('donation_progress_breadcrb_list', 1),
    );
    // TODO: Make selection of cause if none is set!
    $form['summary'] = array(
      '#type' => 'markup',
      '#prefix' => '<div class="donation-box"><div class="donation-box-inner">',
      '#value' => '<label>' . t('Cause') . ': </label><span>' . $node->title . '</span>',
      '#suffix' => '</div></div>',
    );
  }
  // Builds fist page form.
  $form['nid'] = array(
    '#type' => 'hidden',
    '#name' => 'nid',
    '#value' => $node->nid,
  );
  $form['title'] = array(
    '#type' => 'hidden',
    '#name' => 'title',
    '#value' => $node->title,
  );
  $form['donation'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="donation-page-1" class="donation-box">',
    '#suffix' => '</div>',
  );
  $form['donation']['header'] = array(
    '#type' => 'markup',
    '#value' => '<div class="donation-header">' . t('Donate for this cause') . '</div>',
  );
  // Defines $method_init. If no values are selected from the form default is
  // bank_transfer. This var is used to filter the options in the currecy.
  if (!empty($form_state['values']['method'])) {
    $method_init = $form_state['values']['method'];
  }
  elseif (!empty($form_state['storage']['donation']['method'])) {
    $method_init = $form_state['storage']['donation']['method'];
  }
  else {
    $method_init = 'bank_transfer';
  }

  $form['donation']['method'] = array(
    '#type' => 'radios',
    '#name' => 'method',
    //'#prefix' => '<div>' . t('How would you like to donate?') . '</div>',
    '#default_value' => $method_init,
    '#options' => _donations_method_options(),
    '#required' => TRUE,
    // '#description' => t('Please, select how would you like to donate.'),
    '#ahah' => array(
      'path' => 'donate/callback',
      'wrapper' => 'dependent-currency-wrapper',
    ),
    '#prefix' => '<div class="donation-box-inner"><div>' . t('How would you like to donate?') . ' <span class="form-required" title="Задължително поле.">*</span></div>',
  );

  // The CSS for this module hides this next button if JS is enabled.
  $form['donation']['continue'] = array(
    '#type' => 'submit',
    '#value' => t('Choose'),
    '#attributes' => array('class' => 'next-button'),
    '#submit' => array('donations_continue'),
  );

  // Defines $amount_init. If no values are selected from the form default is
  // 5.00.
  if (!empty($form_state['values']['amount'])) {
    $amount_init = $form_state['values']['amount'];
  }
  elseif (!empty($form_state['storage']['donation']['amount'])) {
    $amount_init = $form_state['storage']['donation']['amount'];
  }
  else {
    $amount_init = '5.00';
  }
  $form['donation']['label-amount'] = array(
    '#type' => 'markup',
    '#value' => '<div id="label-amount">' . t('How much would you like to donate?') . ' <span class="form-required" title="Задължително поле.">*</span></div>',
  );
  $form['donation']['details'] = array(
    '#type' => 'item',
    '#prefix' => '<div id="details-wrapper">',
    '#suffix' => '</div>',
  );
  $form['donation']['details']['amount'] = array(
    '#type' => 'textfield',
    '#name' => 'amount',
    '#default_value' => $amount_init,
    '#required' => TRUE,
    '#size' => 15,
    '#prefix' => '<div id="amount-currency-wrapper">',
  );
  $form['donation']['details']['currency'] = array(
    '#type' => 'select',
    '#name' => 'currency',
    '#default_value' => _donations_currency_default_option($method_init, $form_state),
    '#options' => _donations_currency_options($method_init),
    '#tree' => TRUE,
    '#prefix' => '<div id="dependent-currency-wrapper">',
    '#suffix' => '</div></div>',
  );
  $form['donation']['next'] = array(
    '#type' => 'submit',
    '#value' => t('Donate'),
    '#suffix' => '<div class="amount-descr">' . t('Please enter your donation amount using the following number format: 5, 10, 25.50 and choose your currency.') . '</div></div>',
  );
  return $form;
}

/**
 * PAGE 2 - Donation form
 *
 * This form is load as the second step of the donation process.
 * It collects information in the donor
 */
function donations_donation_form_page_two(&$form_state, $node) {
  if ($form_state['storage']['show_page'] == '1') {
    return donations_donation_form_page_one(&$form_state, $node);
  }
  elseif ($form_state['storage']['show_page'] == '3') {
    return donations_donation_form_page_three(&$form_state, $node);
  }

  $form['donation']['progress'] = array(
    '#type' => 'markup',
    '#value' => theme('donation_progress_breadcrb_list', 2),
  );
  $page_two_summary = '<div class="summary-donation-confirmation">';
  $page_two_summary .= '<div class="summary-confirmation-cause"><label>' . t('Cause:') . ' </label><span class="bold">' . $node->title . '</span></div>';
  $page_two_summary .= '<div class="summary-confirmation-amount"><label>' . t('Donation amount:') . ' </label><span class="bold">' . $form_state['storage']['donation']['amount'] . ' ' . $form_state['storage']['donation']['currency'] . '</span></div>';
  $page_two_summary .= '<div class="summary-confirmation-method"><label>' . t('Way of donating:') . ' </label><span class="bold">' . _donations_method_labels($form_state['storage']['donation']['method']) . '</div>';
  $page_two_summary .= '</div>';

  $form['donation']['summary'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="donation-box"><div class="donation-box-inner page-2">',
    '#value' => $page_two_summary,
    '#suffix' => '</div></div>',
  );
  $form['donation']['donor_details'] = array(
    '#type' => 'markup',
    '#value' => '<div class="donation-box">',
  );
  $form['donation']['donor_details']['header'] = array(
    '#type' => 'markup',
    '#value' => '<div class="donation-header">' . t('Tell us more about You') . '</div>',
  );
  $form['donation']['donor_details']['email'] = array(
    '#type' => 'textfield',
    '#name' => 'email',
    '#title' => t('Leave us your e-mail'),
    '#description' => t('We will not send you any spam. We just want to say "Thank you"!'),
    '#default_value' => isset($form_state['storage']['donor']['email']) ? $form_state['storage']['donor']['email'] : '',
    '#required' => TRUE,
    '#prefix' => '<div class="donation-box-inner">',
  );
  $form['donation']['donor_details']['name'] = array(
    '#type' => 'textfield',
    '#name' => 'name',
    '#title' => t('Leave us your first and last name'),
    '#default_value' => isset($form_state['storage']['donor']['name']) ? $form_state['storage']['donor']['name'] : '',
    '#required' => TRUE,
  );
  $form['donation']['donor_details']['phone'] = array(
    '#type' => 'textfield',
    '#name' => 'phone',
    '#title' => t('Leave us your phone number'),
    '#default_value' => isset($form_state['storage']['donor']['phone']) ? $form_state['storage']['donor']['phone'] : '',
    '#required' => TRUE,
  );
  $form['donation']['donor_details']['document'] = array(
    '#type' => 'checkbox',
    '#name' => 'document',
    '#title' => t('I would like to receive an official donation document.'),
    '#default_value' => isset($form_state['storage']['donation']['document']) ? $form_state['storage']['donation']['document'] : '0',
    '#description' => variable_get('donations_document_donor_help', ''),
  );
  $form['donation']['donor_details']['anonym_donor'] = array(
    '#type' => 'checkbox',
    '#name' => 'anonym_donor',
    '#title' => t('I would NOT like my donation to be publicly announced.'),
    '#default_value' => isset($form_state['storage']['donation']['anonym_donor']) ? $form_state['storage']['donation']['anonym_donor'] : '0',
    '#description' => variable_get('donations_anonym_donor_help', ''),
    '#suffix' => '</div>',
  );
  $form['back'] = array(
    "#prefix" => '</div><div id="buttons-box" class="donation-box"><div class="donation-box-inner">',
    '#type' => 'submit',
    '#value' => t('Back'),
  );
  $form['submit_to_checkout'] = array(
    '#type' => 'submit',
    '#value' => t('Next step'),
    '#suffix' => '</div></div>',
  );

  // $form['#submit'][] = 'donations_donation_form_page_two_submit';
  return $form;
}

// TODO: Make methods more pluggable!
// FIXME: Add e-mail support!
/**
 * Donation form PAGE 3
 */
function donations_donation_form_page_three(&$form_state, $node) {
  $op = $form_state['storage']['donation']['method'];
  $path_method = getcwd() . '/' . drupal_get_path('module', 'donations') . '/includes/methods/' . $op . '.inc';
  $path_email = getcwd() . '/' . drupal_get_path('module', 'donations') . '/includes/mail.inc';
  
  switch ($op) {
    case 'bank_transfer' :
      // Load bank tranfer form! TODO: Add instuctions!
      require_once $path_method;
      return donations_bank_transfer_form(&$form_state, $node);
      break;
    case 'epaybg' :
      // Load ePay.bg form! Send mails after the loaded form is submitted.
      require_once $path_method;
      return donations_epaybg_form($form_state);
      break;
    case 'paypal' :
      // Load PayPal form! Send mails after the loaded form is submitted.
      require_once $path_method;
      return donations_paypal_form($form_state);
      break;
    case 'cc' :
      // Load ePay.bg credit card submit form. Send mails after the loaded form
      // is submitted.
      require_once $path_method;
      return donations_epaybg_cc_form($form_state);
      break;
  }
}

/**
 * Form validation
 */
function donations_donation_form_page_one_validate($form, &$form_state) {

  // Validation for the amount field.
  $amount = $form_state['values']['amount'];
  $start = $amount;
  $amount = preg_replace('@[^-0-9\.]@', '', $amount);
  if ($start != $amount) {
    form_set_error('amount', t('Only numbers and decimals are allowed in the amount field'));
  }
  if ($amount && ($amount < 5.00)) {
    form_set_error('amount', t('Enter a valid amount. The minimum amount for a donation is 5.00'));
  }
  if ($form_state['clicked_button']['#id'] == 'edit-back') {
    // Clear the error messages.
    drupal_get_messages('error');
    // Clear the form error state.
    form_set_error(NULL, '', TRUE);
    return TRUE;
  }
  else {
    if ($form_state['storage']['show_page'] == 2) {
      // Validation for the email field.
      $mail = $form_state['values']['email'];
      if (valid_email_address($mail) == 0) {
        form_set_error('email', t('Enter a valid email.'));
      }
      // Validation for the name field.
      $name = $form_state['values']['name'];
      if (count(explode(' ', $name)) < 2) {
        form_set_error('name', t('Enter at least two words in name field.'));
      }

    }
  }

}

/**
 * Submit handeler
 */
function donations_donation_form_page_one_submit(&$form, &$form_state) {
  // Handle page 1 submissions
  // If an AHAH submission, it's just the dependent dropdown working.
  if (!empty($form_state['ahah_submission'])) {
    return;
  }
  $button = $form_state['clicked_button']['#id'];
  switch ($button) {
    // Hendles PAGE 1 submissions.
    case 'edit-next':
      $form_state['storage']['show_page'] = '2';
      $form_state['storage']['donation'] = array(
        'method' => $form_state['values']['method'],
        'amount' => $form_state['values']['amount'],
        'currency' => $form_state['values']['currency'],
        'nid' => $form_state['values']['nid'],
        'title' => $form_state['values']['title'],
      );
      break;
    // Handle PAGE 2 back button.
    case 'edit-back':
      $form_state['storage']['show_page'] = '1';
      $form_state['storage']['donor'] = array(
        'name' => $form_state['values']['name'],
        'email' => $form_state['values']['email'],
        'phone' => $form_state['values']['phone'],
        'document' => $form_state['values']['document'],
        'anonym_donor' => $form_state['values']['anonym_donor'],
      );
      // Clear the error messages.
      drupal_get_messages('error');
      // Clear the form error state.
      form_set_error(NULL, '', TRUE);
      break;
    // Handle PAGE 2 submissions.
    case 'edit-submit-to-checkout':
      $form_state['storage']['show_page'] = '3';
      $form_state['storage']['donation']['document'] = $form_state['values']['document'];
      $form_state['storage']['donation']['anonym_donor'] = $form_state['values']['anonym_donor'];
      $donation = $form_state['storage']['donation'];
      $form_state['storage']['donor'] = array(
        'name' => $form_state['values']['name'],
        'email' => $form_state['values']['email'],
        'phone' => $form_state['values']['phone'],
      );
      $donor = $form_state['storage']['donor'];
      extract($donor);
      extract($donation);
      $resultset = db_query("SELECT donor_id FROM {donations_donors} WHERE email = '%s'", $email);
      $donor_id = db_result($resultset);

      if ($donor_id == FALSE) {
        // Inserts donor datails into the data base.
        db_query("INSERT INTO {donations_donors} (email, name, phone) VALUES ('%s', '%s', '%s')", check_plain($email), check_plain($name), check_plain($phone));
        $resultset = db_query("SELECT donor_id FROM {donations_donors} WHERE email = '%s'", $email);
        $donor_id = db_result($resultset);
      }
      // Inserts donation details to {donations_donations}.
      db_query("INSERT INTO {donations_donations} (nid, amount, currency, method, donor_id, document, anonymous, date) VALUES ('%d', '%f', '%s', '%s','%d', '%d', '%d', '%d')", check_plain($nid), check_plain($amount), check_plain($currency), check_plain($method), check_plain($donor_id), check_plain($document), check_plain($anonym_donor), time());
      $donation['donation_id'] = db_last_insert_id('donations_donations', 'donation_id');
      $donor['donor_id'] = $donor_id;
      $form_state['storage']['donation']['donation_id'] = $donation['donation_id'];
      $form_state['storage']['donor']['donor_id'] = $donor['donor_id'];
      $donation_summary = array_merge($form_state['storage']['donor'], $form_state['storage']['donation']);
      // $donation_summary[] = $form_state['storage']['donation'];
      $form_state['storage']['donation_summary'] = $donation_summary;
      break;
    // Handles Page 3 bank transfer finish button.
    case 'edit-finish':
      $path_email = getcwd() . '/' . drupal_get_path('module', 'donations') . '/includes/mail.inc';
      require_once $path_email;
      donations_mail_send($form_state['storage']['donation_summary'], 'mail_donor');
      donations_mail_send($form_state['storage']['donation_summary'], 'mail_admin');
      dsm($form_state);
      $method = $form_state['storage']['donation_summary']['method'];
      $data = http_build_query($form_state['values'], '', '&');
      switch ($method) {
        case 'epaybg':
          // Set some parameters for sending request.
          $request_url = variable_get('donations_epaybg_mode', 'https://www.epay.bg/');
          break;
        case 'paypal':
          // Set some parameters for sending request.
          $request_url = variable_get('donations_paypal_mode', 'https://www.paypal.com/cgi-bin/webscr');
          break;
        case 'cc':
          // Set some parameters for sending request.
          $request_url = 'https://www.epay.bg/'; //variable_get('donations_epaybg_mode', 'https://www.epay.bg/');
          break;
      }
      unset($form_state['storage']);
      if ($method == 'epaybg' || $method == 'paypal' || $method == 'cc') {
        $form_state['redirect'] = url($request_url, array(
          'query' => $data,
          'external' => TRUE,
        ));
      }
      break;
    case 'edit-cancel':
      unset($form_state['storage']);
      $form_state['redirect'] = 'donations';
      break;
  }
}

/**
 * Submit handler for 'continue_to_dependent_dropdown'.
 */
function donations_continue($form, &$form_state) {
  $values = $form_state['values'];
  unset($form_state['submit_handlers']);
  form_execute_handlers('submit', $form, $form_state);
  $form_state['my_values'] = $values;
  $form_state['rebuild'] = TRUE;
}

/**
 * The AHAH callback. It processes the form using donations_callback_helper()
 */
function donations_callback() {
  $form = donations_callback_helper();

  $changed_elements = $form['donation']['details']['currency'];
  // Prevent duplicate wrappers.
  unset($changed_elements['#prefix'], $changed_elements['#suffix']);

  //$output = theme('status_messages') . drupal_render($changed_elements);
  $output = drupal_render($changed_elements);
  drupal_json(array(
    'status' => TRUE,
    'data' => $output,
  ));
}

/**
 * Helper function to populate the first dropdown. This would normally be
 * pulling data from the database.
 *
 * @return array
 *   an array of options
 */
function _donations_method_options() {
  return array(
    'bank_transfer' => '<span>' . t('Bank transfer') . '</span>',
    'epaybg' => '<span>' . t('ePay.bg') . '</span>',
    'paypal' => '<span>' . t('PayPal') . '</span>',
    'cc' => '<span>' . t('Credit or Debit card') . '</span>',
  );
}

/**
 * Dynamicaly generates availabe currency options based on the selected method.
 *
 * @param sting $method_init
 *   selected method
 *
 * @return array
 *   array of currency options
 */
function _donations_currency_options($method_init) {
  switch ($method_init) {
    case 'bank_transfer' :
      return array(
        'BGN' => 'BGN',
        'USD' => 'USD',
        'EUR' => 'EUR',
        'GBP' => 'GBP',
      );
      break;
    case 'epaybg' :
      return array(
        'BGN' => 'BGN',
      );
      break;
    case 'paypal' :
      return array(
        'USD' => 'USD',
        'EUR' => 'EUR',
        'GBP' => 'GBP',
      );
    case 'cc' :
      return array(
        'BGN' => 'BGN',
        'USD' => 'USD',
        'EUR' => 'EUR',
      );
      break;
  }
}

/**
 * Dynamicaly set defauls currency based on selected methods
 *
 * @param string $method_init
 *   sellected method
 *
 * @return string
 *   currency
 */
function _donations_currency_default_option($method_init, $form_state) {
  switch ($method_init) {
    case 'bank_transfer' :
      return isset($form_state['storage']['donation']['currency']) ? $form_state['storage']['donation']['currency'] : 'BGN';
      break;
    case 'epaybg' :
      return isset($form_state['storage']['donation']['currency']) ? $form_state['storage']['donation']['currency'] : 'BGN';
      break;
    case 'paypal' :
      return isset($form_state['storage']['donation']['currency']) ? $form_state['storage']['donation']['currency'] : 'USD';
    case 'cc' :
      return isset($form_state['storage']['donation']['currency']) ? $form_state['storage']['donation']['currency'] : 'BGN';
      break;
  }
}
