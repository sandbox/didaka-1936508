<?php
/**
 * @file
 * System that send e-mails to donors and site administrators.
 */

/**
 * Implements of hook_mail().
 *
 * This hook defines a list of possible e-mail templates that this module can
 * send. Each e-mail is given a unique identifier, or 'key'.
 *
 * $message comes in with some standard properties already set: 'to' address,
 * 'from' address, and a set of default 'headers' from drupal_mail(). The goal
 * of hook_mail() is to set the message's 'subject' and 'body' properties, as
 * well as make any adjustments to the headers that are necessary.
 *
 * The $params argument is an array which can hold any additional data required
 * to build the mail subject and body; for example, user-entered form data, or
 * some context information as to where the mail request came from.
 *
 * Note that hook_mail() is not actually a hook. It is only called for a single
 * module, the module named in the first argument of drupal_mail(). So it's
 * a callback of a type, but not a hook.
 */
function donations_mail($key, &$message, $params) {
  switch ($key) {
    case 'mail_donor':
      $message['subject'] = _donations_summary($params['values'], array('donations_email_subject', 'Thank you for donating!'));
      // The message passed from $params.
      $message['body'][] = $params['message'];
      break;
    case 'mail_admin':
      $message['subject'] = t('There is a new dontaion!');
      $message['body'][] = t('A new donation was made through the donation section.');
      $message['body'][] = t('The donation details are:');
      // The message passed from $params.
      $message['body'][] = $params['message'];
      break;
  }
}

/**
 * Sends e-mails
 *
 * @param array $values
 *   What will be sent
 * @param string $key
 *   a key for donation email
 */
function donations_mail_send($values, $key) {
  // All system mails need to specify the module and template key (mirrored from
  // hook_mail()) that the message they want to send comes from.
  $module = 'donations';

  // Specify 'to' and 'from' addresses.
  switch ($key) {
    case 'mail_donor':
      $to = $values['email'];
      break;
    case 'mail_admin';
      $to = variable_get('donations_admin_email', '');
      break;
  }

  $from = variable_get('donations_from_email', variable_get('donations_admin_email', variable_get('site_mail', 'admin@example.com')));

  global $language;
  $lang = $language->language;
  $send = TRUE;
  // "params" loads in additional context for email content completion in
  // hook_mail(). In this case, we want to pass in the values the user entered
  // into the form, which include the message body in $form_values['message'].
  $message = _donations_summary($values, array('donations_' . $values['method'] . '_donor_email', ''));
  $params['message'] = $message;
  $params['values'] = $values;

  $result = drupal_mail($module, $key, $to, $lang, $params, $from, $send);
  if ($result['result'] == TRUE) {
    if ($key == 'mail_donor') {
      drupal_set_message(t('An e-mail has been sent with all details for your donation.'));
    }
  }
  else {
    drupal_set_message(t('There was a problem sending your message and it was not sent.'), 'error');
  }
}
