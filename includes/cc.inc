<?php
/**
 * @file
 */

/**
 * ePay.bg summary form
 *
 * @param array $form_state
 *   form state
 */
function donations_epaybg_cc_form($form_state) {
  extract($form_state['storage']['donation']);
  extract($form_state['storage']['donor']);
  $min = variable_get('donations_epaybg_min', 'Enter MIN');
  $secret = variable_get('donations_epaybg_secret_key', 'Enter Secret key');
  $exp_date = date('d.m.Y', time() + 60 * 60 * 24 * 30);
  $descr = drupal_substr(t('Donation for !title', array('!title' => $title)), 0, 100);

  global $language;
  $lang_name = $language->language;

  $data = <<<DATA
MIN={$min}
INVOICE={$donation_id}
AMOUNT={$amount}
EXP_TIME={$exp_date}
DESCR={$descr}
CURRENCY={$currency}
ENCODING=utf-8
DATA;

  $ENCODED = base64_encode($data);
  // SHA-1 algorithm REQUIRED!
  $CHECKSUM = _donations_hmac('sha1', $ENCODED, $secret);

  $form['progress'] = array(
    '#type' => 'markup',
    '#value' => _donations_progress_breadcrb(3),
  );
  $form['summary'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="buttons-box" class="donation-box"><div class="donation-box-inner">',
    '#value' => _donations_confirmation_summary($form_state['storage']['donation_summary']),
    '#suffix' => '</div></div>',
  );
  $form['instructions'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="donation-box"><div class="donation-box-inner">',
    '#value' => variable_get('donations_cc_instructions', ''),
    '#suffix' => '</div></div>',
  );
  $form['page'] = array(
    '#type' => 'hidden',
    '#name' => 'PAGE',
    '#value' => 'credit_wt',
  );
  $form['LANG'] = array(
    '#type' => 'hidden',
    '#name' => 'LANG',
    '#value' => $lang_name,
  );
  $form['ENCODED'] = array(
    '#type' => 'hidden',
    '#name' => 'ENCODED',
    '#value' => $ENCODED,
  );
  $form['CHECKSUM'] = array(
    '#type' => 'hidden',
    '#name' => 'CHECKSUM',
    '#value' => $CHECKSUM,
  );
  $form['URL_OK'] = array(
    '#type' => 'hidden',
    '#name' => 'URL_OK',
    '#value' => variable_get('donations_epaybg_url_ok', 'Enter URL_OK'),
  );
  $form['URL_CANCEL'] = array(
    '#type' => 'hidden',
    '#name' => 'URL_CANCEL',
    '#value' => variable_get('donations_epaybg_url_cancel', 'Enter URL_CANCEL'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#name' => 'cancel',
    '#value' => t('Cancel'),
    '#prefix' => '<div id="buttons-box" class="donation-box"><div class="donation-box-inner">',
  );
  $form['finish'] = array(
    '#type' => 'submit',
    '#name' => 'finish',
    '#value' => t('Finish'),
    '#suffix' => '</div></div>',

  );


  return $form;
}
