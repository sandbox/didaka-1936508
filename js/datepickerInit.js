/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Drupal.behaviors.donationsdatepickerInit = function(context) {
  (function($){
    // Adds datepcher on the node edit.
    $('#edit-last-until-field').datepicker({
      dateFormat : 'dd/mm/yy',
      changeMonth : true,
      changeYear : true
    });
  })(jQuery);
};
