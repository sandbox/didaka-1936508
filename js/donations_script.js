Drupal.behaviors.donations = function(context) {
  (function($){
    // Adds datepcher on the node edit.
    //    $('#edit-last-until-field').datepicker({
    //      dateFormat : 'dd/mm/yy',
    //      changeMonth : true,
    //      changeYear : true
    //    });
  
    // Tootips on donation form.
    $('#edit-document-wrapper .option, #edit-anonym-donor-wrapper .option').append('<span class="donor-help">');
    $('.donor-help').hover(function(e){
      var x = e.pageX;
      var y = e.pageY - $(window).scrollTop();
      $(this).parent().siblings('.description').css({
        'top' : y, 
        'left': x
      });
      $(this).parent().siblings('.description').stop().show(0);
    },function(){
      $(this).parent().siblings('.description').stop().hide(0);
    });
  
    // Show/Hide fields on node edit form when active or not.
    if($('#edit-active-field-wrapper').length > 0){
      $('#edit-total-goal-field-wrapper label').append('<span class="form-required" title="' + Drupal.t('This field is required.') + '">*</span>');
      $('#edit-active-field-wrapper input').change(function () {
        if($('#edit-active-field-wrapper input').is(':checked')){
          $('.donations-group-wrapper').css('display', 'block');
          
        } else {
          $('.donations-group-wrapper').css('display', 'none');
        }       
      }).change();
    }
  })(jQuery);
  
};
