<?php 

/**
 * @file
 * Displays a Donations progress bar.
 *
 * Available variables:
 *
 * General utility variables:
 * - $goal: The funds neeted. The goal.
 * - $funds_raised: An array of CSS files for the current page.
 * - $percent_view: The directory the theme is located in, e.g. themes/garland or
 * - $positon: TRUE if the current page is the front page.
 * - $marker: TRUE if the user is registered and signed in.
 * - $percent: TRUE if the user has permission to access administration pages.
 *
 * @see template_preprocess()
 * @see template_preprocess_donation_progress_bar()
 */
?>
<div id="donation-progress-bar" class="clearfix">
  <div class="funds-needed"><?php print t('Funds needed: <b>!goal BGN</b>', array('!goal' => $goal)); ?> </div>
  <div class="donation-progress">
    <div class="wrapper">
      <div class="bar">
        <div style="width:<?php print $percent_view; ?>%" class="progress"></div>
        <div class="current_total <?php print $positon . ' ' . $marker ?>" style ="<?php print $positon . ':' . $positon_value; ?>%">
          <div class="current_total_inner">
            <span class="percentage"><?php print $percent; ?>%</span>
            <span class="amount"><?php print t('Funds raised: <b>!raised BGN</b>', array('!raised' => $funds_raised)); ?></span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>